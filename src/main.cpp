#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include <string>
#include <map>
#include "wireless.h"
using namespace std;

map<string, pair<string, int>> beacon_table;

void usage()
{
    printf("syntax : airodump <interface>\n");
    printf("sample : airodump mon0\n");
}

void printAirodumpTable()
{
    system("clear");
    printf("BSSID\t\t\tBeacons\tESSID\n\n");
    for (const auto &entry : beacon_table)
    {
        const string &bssid = entry.first;
        const pair<string, int> &data = entry.second;

        printf("%s\t", bssid.c_str());
        printf("%7d\t", data.second);
        printf("%s\n", data.first.c_str());
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        usage();
        return -1;
    }

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_live(argv[1], BUFSIZ, 1, 1, errbuf);
    if (handle == nullptr)
    {
        fprintf(stderr, "couldn't open device %s(%s)\n", argv[1], errbuf);
        return -1;
    }

    printAirodumpTable();

    while (true)
    {
        struct pcap_pkthdr *hdr;
        const u_char *pckt;

        int res = pcap_next_ex(handle, &hdr, &pckt);
        if (res == 0)
            continue;

        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
            break;
        }

        ieee80211_radiotap_header *radiotap = (ieee80211_radiotap_header *)pckt;
        ieee80211_MAC_header *beacon = (ieee80211_MAC_header *)(pckt + radiotap->it_len);
        if (beacon->type != ieee80211_MAC_header::BEACON)
            continue;
        fixed_parameters *fixed = (fixed_parameters *)(pckt + radiotap->it_len + sizeof(struct ieee80211_MAC_header));
        tagged_parameters *tagged = (tagged_parameters *)(pckt + radiotap->it_len + sizeof(struct ieee80211_MAC_header) + 12);
        string bssid= string(beacon->bssid);
        if (beacon_table.count(bssid))
            beacon_table[bssid].second++;
        
        else
        {
            const u_char *essid_ptr = reinterpret_cast<const u_char*>(tagged) + 2;
            string essid;
            for (uint8_t i = 0; i < tagged->tag_len; i++)
                essid.push_back(*essid_ptr++);
            beacon_table.insert({bssid, {essid, 1}});
        }

        printAirodumpTable();
    }

    pcap_close(handle);
    
    return 0;
}

